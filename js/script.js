/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */

/*****************************************************************
                PRELOADER FUNCTION
*****************************************************************/
$(window).on('load', function () {
    $("#preloader").delay(500).fadeOut("slow");
});

/*****************************************************************
            TEAM SECTION OWL CAROUSEL FUNCTION
*****************************************************************/
$(document).ready(function () {
    $("#team-right").owlCarousel({
        items: 2,
        autoplay: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>']
    });
});

/*****************************************************************
                PRELOADER FUNCTION
*****************************************************************/

$(document).ready(function () {
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {
            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 800);
        });
        this.destroy();
    }, {
        offset: 'bottom-in-view'
    });
});

$('#services-tabs').responsiveTabs({
    //    startCollapsed: 'accordion'
    animation: 'slide'
});


/*****************************************************************
                PORTFOLIO SECTION
*****************************************************************/
$(document).ready(function () {
    $("#isotope-container").isotope({});


    $("#isotope-filters").on("click", "button", function () {
        let filterValue = $(this).attr("data-filter");
        //console.log(filterValue);
        $("#isotope-container").isotope({
            filter: filterValue
        });

        //active button        
        $("#isotope-filters").find('.active').removeClass('active');
        $(this).addClass('active');
    });
});

$(document).ready(function () {
    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',

            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement :
                    openerElement.find('img');
            }
        }

    })
});
/*****************************************************************
            TESTIMONIAL SECTION OWL CAROUSEL FUNCTION
*****************************************************************/
$(document).ready(function () {
    $("#testimonial-slider").owlCarousel({
        items: 1,
        autoplay: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>']
    });
});


/*****************************************************************
            CLIENT SECTION OWL CAROUSEL FUNCTION
*****************************************************************/
$(document).ready(function () {
    $("#client-names").owlCarousel({
        items: 6,
        autoplay: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>'],
        responsive:{
            0:{
                items: 2
            },
            480:{
                items: 3,
                nav: false
            },
            768:{
                items: 6,
            }
                                   
        }
        
    });
});

/*****************************************************************
            STATS
*****************************************************************/

$(function () {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});


/*****************************************************************
            MAP 
*****************************************************************/

$(window).on('load',function(){
   var addressString = " Harsha aparts , Mulund, Maharashtra, India";
    var myLatLng={
        lat: 19.180322,
        lng:72.949621
    };
    var myMap = new google.maps.Map(document.getElementById('map'),{
        zoom: 17,
        center: myLatLng
    });
    
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: myMap,
        title: 'click to see address'
    });
    
    var infoWindow = new google.maps.InfoWindow({
        content: addressString
    });
    
    marker.addListener('click',function(){
        infoWindow.open(myMap,marker)
    })
});
